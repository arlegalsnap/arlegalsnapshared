(function(ng) {

	"use strict";

	ng.module("SnapApp").controller("ContentController", ContentController);

	ContentController.$inject = ["ColorService", "CategoryService", "resolvePage"];

	function ContentController(ColorService, CategoryService, resolvePage) {

		var vm = this;

		vm.page = resolvePage;

		ColorService.removeColor();
		CategoryService.setCurrentCategoryName("");
	}
}(angular));
