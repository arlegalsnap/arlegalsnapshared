(function(ng) {

	"use strict";

	ng.module("SnapApp").directive("imageViewer", imageViewer);

	function imageViewer() {

		return {

			restrict: "E",
			scope: true,
			templateUrl: "client/app/directives/image-viewer/image-viewer.html",
			link: function(scope, element, attribute) {

				// Needed since attr value is Angular data bind
				attribute.$observe("images", function(value) {

					var paused = false;

					if (value) {

						// Array comes through as string. Split and parse it back to array
						scope.images = JSON.parse(value.split(","));
						// Filter out empty strings (found in products without images)
						scope.images = scope.images.map(function(element) {

							if (element.length) return element;
						});
						scope.imageCount = (scope.images[0]) ? scope.images.length : 0;
						scope.currentIndex = 0;
						scope.next = function() {

							scope.currentIndex = (scope.currentIndex + 1 !== scope.imageCount) ? scope.currentIndex + 1 : 0;
						};
						scope.previous = function() {

							scope.currentIndex = (scope.currentIndex !== 0) ? scope.currentIndex - 1 : scope.imageCount - 1;
						};
						scope.pause = function() { paused = true; };
						scope.unpause = function() { paused = false; };

						autoIncrement();
					}

					function autoIncrement() {

						setTimeout(function() {

							if (!paused) scope.next();
							scope.$apply();
							autoIncrement();
						}, 3000);
					}
				});
			}
		};
	}
}(angular));
