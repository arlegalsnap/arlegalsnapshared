(function(ng) {

	"use strict";

	ng.module("SnapApp").directive("howItWorks", howItWorks);

	function howItWorks() {

		return {

			restrict: "E",
			templateUrl: "client/app/directives/how-it-works/how-it-works.html"
		};
	}
}(angular));
