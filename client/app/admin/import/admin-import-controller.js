(function(ng) {

	"use strict";

	ng.module("SnapApp").controller("AdminImportController", AdminImportController);

	AdminImportController.$inject = ["ColorService", "LoadingService", "AdminImportService"];

	function AdminImportController(ColorService, LoadingService, AdminImportService) {

		var vm = this;

		vm.importData = importData;

		ColorService.removeColor();

		function importData() {

			if (!LoadingService.isLoading()) {

				AdminImportService.importData();
			}
		}
	}
}(angular));
