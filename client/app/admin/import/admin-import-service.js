(function(ng) {

	"use strict";

	ng.module("SnapApp").factory("AdminImportService", AdminImportService);

	AdminImportService.$inject = ["$http", "$location", "LoadingService"];

	function AdminImportService($http, $location, LoadingService) {

		return {

			importData: function() {

				LoadingService.startLoading();
				$http.get("server/admin/importData.php").then(function() {

					LoadingService.stopLoading();
					$location.path("admin/home");
				});
			}
		};
	}
}(angular));
