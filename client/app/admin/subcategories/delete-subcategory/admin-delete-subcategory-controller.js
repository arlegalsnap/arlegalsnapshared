(function(ng) {

	"use strict";

	ng.module("SnapApp").controller("AdminDeleteSubcategoryController", AdminDeleteSubcategoryController);

	AdminDeleteSubcategoryController.$inject = ["$routeParams", "ColorService", "resolveCategories", "AdminSubcategoriesService", "CategoryService"];

	function AdminDeleteSubcategoryController($routeParams, ColorService, resolveCategories, AdminSubcategoriesService, CategoryService) {

		var vm = this,
			cm = {};

		cm.subcategoryTimestamp = $routeParams.timestamp;
		cm.categories = resolveCategories;

		vm.buttonDisabled = false;
		vm.subcategoryName = CategoryService.getSubcategoryNameGivenSubcategoryTimestamp(cm.subcategoryTimestamp, cm.categories);
		vm.deleteSubcategory = deleteSubcategory;

		ColorService.setCurrentColor("red");

		function deleteSubcategory() {

			vm.buttonDisabled = true;
			AdminSubcategoriesService.deleteSubcategory(cm.subcategoryTimestamp);
		}
	}
}(angular));
