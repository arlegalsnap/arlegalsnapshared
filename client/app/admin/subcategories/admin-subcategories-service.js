(function(ng) {

	"use strict";

	ng.module("SnapApp").factory("AdminSubcategoriesService", AdminSubcategoriesService);

	AdminSubcategoriesService.$inject = ["$http", "$location", "LoadingService", "DataService"];

	function AdminSubcategoriesService($http, $location, LoadingService, DataService) {

		var services = {

			createSubcategory: createSubcategory,
			deleteSubcategory: deleteSubcategory,
			editSubcategory: editSubcategory
		};

		return services;

		function createSubcategory(categoryTimestamp, subcategoryName) {

			var subcategoryTimestamp = Date.now();
			LoadingService.startLoading();
			$http.post("server/admin/createSubcategory.php", { "categoryTimestamp": categoryTimestamp, "subcategoryName": subcategoryName, "subcategoryTimestamp": subcategoryTimestamp }).then(function(response) {

				LoadingService.stopLoading();
				if (response.data === "success") {

					DataService.clearCategoriesCache();
					$location.path("admin/subcategories");
				}
			});
		}

		function deleteSubcategory(subcategoryTimestamp) {

			LoadingService.startLoading();
			$http.get("server/admin/deleteSubcategory.php?timestamp=" + subcategoryTimestamp).then(function(response) {

				LoadingService.stopLoading();
				if (response.data === "success") {

					DataService.clearCategoriesCache();
					DataService.clearProductsCache();
					$location.path("admin/subcategories");

				} else {

					//re-enable form?;
				}
			});
		}

		function editSubcategory(categoryTimestamp, subcategoryName, subcategoryTimestamp) {

			LoadingService.startLoading();
			$http.post("server/admin/editSubcategory.php", { "categoryTimestamp": categoryTimestamp, "subcategoryName": subcategoryName, "subcategoryTimestamp": subcategoryTimestamp }).then(function(response) {

				LoadingService.stopLoading();
				if (response.data === "success") {

					DataService.clearCategoriesCache();
					$location.path("admin/subcategories");

				} else {

					//re-enable form?;
				}
			});
		}
	}
}(angular));
