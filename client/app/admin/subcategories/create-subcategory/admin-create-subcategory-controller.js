(function(ng) {

	"use strict";

	ng.module("SnapApp").controller("AdminCreateSubcategoryController", AdminCreateSubcategoryController);

	AdminCreateSubcategoryController.$inject = ["ColorService", "resolveCategories", "AdminSubcategoriesService"];

	function AdminCreateSubcategoryController(ColorService, resolveCategories, AdminSubcategoriesService) {

		var vm = this;

		vm.categories = resolveCategories;
		vm.createSubcategory = createSubcategory;

		ColorService.setCurrentColor("red");

		function createSubcategory(isValid) {

			if (isValid) {

				AdminSubcategoriesService.createSubcategory(vm.categoryTimestamp, vm.subcategoryName);

			} else {

				alert("Please complete all required fields.");
			}
		}
	}
}(angular));
