(function(ng) {

	"use strict";

	ng.module("SnapApp").controller("AdminEditSubcategoryController", AdminEditSubcategoryController);

	AdminEditSubcategoryController.$inject = ["$routeParams", "ColorService", "resolveCategories", "AdminSubcategoriesService", "CategoryService"];

	function AdminEditSubcategoryController($routeParams, ColorService, resolveCategories, AdminSubcategoriesService, CategoryService) {

		var vm = this,
			cm = {};

		vm.formDisabled = 			true;
		vm.subcategoryTimestamp = 	$routeParams.timestamp;
		vm.formDisabled = 			false;
		vm.categories = 			resolveCategories;
		vm.categoryName = 			CategoryService.getCategoryNameForSubcategoryTimestamp(vm.subcategoryTimestamp, vm.categories);
		cm.category = 				CategoryService.getCategoryFromCategories(vm.categoryName, vm.categories);
		vm.categoryTimestamp = 		cm.category.timestamp;
		vm.subcategoryName = 		CategoryService.getSubcategoryNameGivenSubcategoryTimestamp(vm.subcategoryTimestamp, vm.categories);
		vm.editSubcategory = 		editSubcategory;

		ColorService.setCurrentColor("red");

		function editSubcategory(isValid) {

			if (isValid) {

				AdminSubcategoriesService.editSubcategory(vm.categoryTimestamp, vm.subcategoryName, vm.subcategoryTimestamp);

			} else {

				alert("Please complete all required fields.");
			}
		}
	}
}(angular));
