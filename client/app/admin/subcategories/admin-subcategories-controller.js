(function(ng) {

	"use strict";

	ng.module("SnapApp").controller("AdminSubcategoriesController", AdminSubcategoriesController);

	AdminSubcategoriesController.$inject = ["ColorService", "resolveCategories"];

	function AdminSubcategoriesController(ColorService, resolveCategories) {

		var vm = this;

		vm.categories = resolveCategories;

		ColorService.setCurrentColor("red");
	}
}(angular));
