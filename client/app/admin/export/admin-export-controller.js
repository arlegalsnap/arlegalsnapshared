(function(ng) {

	"use strict";

	ng.module("SnapApp").controller("AdminExportController", AdminExportController);

	AdminExportController.$inject = ["ColorService", "LoadingService", "AdminExportService"];

	function AdminExportController(ColorService, LoadingService, AdminExportService) {

		var vm = this;

		vm.message;
		vm.exportData = exportData;

		ColorService.removeColor();

		function exportData() {

			if (!LoadingService.isLoading()) {

				AdminExportService.exportData(vm.message);
			}
		}
	}
}(angular));
