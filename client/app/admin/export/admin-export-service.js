(function(ng) {

	"use strict";

	ng.module("SnapApp").factory("AdminExportService", AdminExportService);

	AdminExportService.$inject = ["$http", "$location", "LoadingService"];

	function AdminExportService($http, $location, LoadingService) {

		return {

			exportData: function(message) {

				LoadingService.startLoading();
				$http.post("server/admin/exportData.php", { message: message }).then(function() {

					LoadingService.stopLoading();
					$location.path("admin/home");
				});
			}
		};
	}
}(angular));
