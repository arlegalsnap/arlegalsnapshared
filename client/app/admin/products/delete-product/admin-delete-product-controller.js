(function(ng) {

	"use strict";

	ng.module("SnapApp").controller("AdminDeleteProductController", AdminDeleteProductController);

	AdminDeleteProductController.$inject = ["$routeParams", "ColorService", "resolveProduct", "AdminProductsService"];

	function AdminDeleteProductController($routeParams, ColorService, resolveProduct, AdminProductsService) {

		var vm = this;

		vm.buttonDisabled = false;
		vm.productTimestamp = $routeParams.timestamp;
		vm.product = resolveProduct;
		vm.deleteProduct = deleteProduct;

		ColorService.setCurrentColor("blue");

		function deleteProduct() {

			vm.buttonDisabled = true;
			AdminProductsService.deleteProduct(vm.productTimestamp);
		}
	}
}(angular));
