(function(ng) {

	"use strict";

	ng.module("SnapApp").factory("AdminProductsService", AdminProductsService);

	AdminProductsService.$inject = ["$http", "$location", "LoadingService", "DataService"];

	function AdminProductsService($http, $location, LoadingService, DataService) {

		var service = {

			createProduct: createProduct,
			buildUsesString: buildUsesString,
			formatUses: formatUses,
			deleteProduct: deleteProduct,
			buildSubcategoriesArray: buildSubcategoriesArray,
			buildSubcategoriesObject: buildSubcategoriesObject,
			buildSubcategoriesString: buildSubcategoriesString,
			buildImagesString: buildImagesString,

		};

		return service;

		function createProduct(newProduct) {

			LoadingService.startLoading();
			$http.post("server/admin/createProduct.php", { "newProduct": newProduct }).then(function() {

				DataService.clearProductsCache();
				$location.path("admin/product/edit/" + newProduct.timestamp);
			});
		}

		function buildUsesString(usesArray) {

			var usesString = "";

			ng.forEach(usesArray, function(thisUse) {

				usesString += "---" + thisUse.value;
			});

			return usesString.substring(3);
		}

		function formatUses(uses) {

			var formattedUses = [];

			ng.forEach(uses, function(thisUse) {

				formattedUses.push({ "value": thisUse });
			});

			return formattedUses;
		}

		function deleteProduct(productTimestamp) {

			$http.get("server/admin/deleteProduct.php?timestamp=" + productTimestamp).then(function(response) {

				if (response.data === "success") {

					DataService.clearProductDetailsCache(productTimestamp);
					DataService.clearProductsCache();
					$location.path("admin/products");

				} else {

					// re-enable form?
				}
			});
		}

		function buildSubcategoriesArray(subcategoriesObject) {

			var subcategoriesArray = [];

			ng.forEach(subcategoriesObject, function(value, key) {

				subcategoriesArray.push("" + key);
			});

			return subcategoriesArray;
		}

		function buildSubcategoriesObject(subcategories) {

			if (subcategories.length > 0) {

				var subcategoriesString = "" + subcategories,
					subcategoriesArray = subcategoriesString.split(","),
					subcategoriesObject = {};

				ng.forEach(subcategoriesArray, function(thisSubcategory) {

					subcategoriesObject[thisSubcategory] = true;
				});

				return subcategoriesObject;
			}
		}

		function buildSubcategoriesString(subcategoriesObject) {

			var subcategoriesString = "";

			ng.forEach(subcategoriesObject, function(value, key) {

				if (value && key.length > 0) subcategoriesString += ("," + key);
			});

			return subcategoriesString.substring(1);
		}

		function buildImagesString(imageCount, timestamp) {

			var imagePathPrefix = "server/data/products/",
				imagesString = "";

			for (var x = 0; x < imageCount; x++) {

				imagesString += ("," + imagePathPrefix + timestamp + "/" + x + ".png");
			}

			return imagesString.substring(1);
		}
	}
}(angular));
