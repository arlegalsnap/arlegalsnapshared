(function(ng) {

	"use strict";

	ng.module("SnapApp").controller("AdminCreateProductController", AdminCreateProductController);

	AdminCreateProductController.$inject = ["$filter", "ColorService", "AdminProductsService"];

	function AdminCreateProductController($filter, ColorService, AdminProductsService) {

		var vm = this;

		vm.formDisabled = false;
		vm.newProduct = {};
		vm.newProduct.timestamp = Date.now();
		vm.newProduct.uses = [{ "value": "" }];
		vm.newProduct.interview = "../../data/products/" + vm.newProduct.timestamp + "/interview.hdpkg";
		vm.addUse = addUse;
		vm.removeUse = removeUse;
		vm.createProduct = createProduct;

		ColorService.setCurrentColor("blue");

		function addUse() {

			vm.newProduct.uses.push({ "value": "" });
		}

		function removeUse() {

			vm.newProduct.uses.pop();
		}

		function createProduct(isValid) {

			if (isValid) {

				vm.formDisabled = true;
				vm.newProduct.price = $filter("PriceToFloat")(vm.newProduct.price);
				vm.newProduct.usesString = AdminProductsService.buildUsesString(vm.newProduct.uses);
				AdminProductsService.createProduct(vm.newProduct);

			} else {

				alert("Please complete all required fields.");
			}
		}
	}
}(angular));
