(function(ng) {

	"use strict";

	ng.module("SnapApp").factory("AdminEditProductService", AdminEditProductService);

	AdminEditProductService.$inject = ["$http", "$location", "Upload", "LoadingService", "DataService"];

	function AdminEditProductService($http, $location, Upload, LoadingService, DataService) {

		var service = {

			uploadInterview: uploadInterview,
			uploadImage: uploadImage,
			editProduct: editProduct
		};

		return service;

		function uploadInterview(file, timestamp) {

			LoadingService.startLoading();
			Upload.upload({

				url: "server/admin/uploadInterview.php",
				fields: { "timestamp": timestamp },
				file: file

			}).success(function() {

				LoadingService.stopLoading();
			});
		}

		function uploadImage(file, timestamp, index) {

			LoadingService.startLoading();
			Upload.upload({

				url: "server/admin/uploadImage.php",
				fields: { "index": index, "timestamp": timestamp },
				file: file

			}).success(function() {

				LoadingService.stopLoading();
			});
		}

		function editProduct(product) {

			LoadingService.startLoading();
			$http.post("server/admin/editProduct.php", product).then(function(response) {

				LoadingService.stopLoading();
				DataService.clearProductsCache();
				DataService.clearProductDetailsCache(product.ts);
				if (response.data === "success") {

					$location.path("admin/products");
				}
			});
		}
	}
}(angular));
