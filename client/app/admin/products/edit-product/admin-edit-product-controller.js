(function(ng) {

	"use strict";

	ng.module("SnapApp").controller("AdminEditProductController", AdminEditProductController);

	AdminEditProductController.$inject = ["$routeParams", "ColorService", "AdminProductsService", "AdminEditProductService", "LoadingService", "resolveCategories", "resolveProduct"];

	function AdminEditProductController($routeParams, ColorService, AdminProductsService, AdminEditProductService, LoadingService, resolveCategories, resolveProduct) {

		var vm = this,
			cm = {};

		cm.timestamp = $routeParams.timestamp;

		vm.formDisabled = false;
		vm.categories = resolveCategories;
		vm.imageFields = [];
		vm.product = resolveProduct;
		vm.product.uses = AdminProductsService.formatUses(vm.product.uses);
		vm.product.subcategoriesObject = AdminProductsService.buildSubcategoriesObject(vm.product.subcategories);
		vm.uploadInterview = uploadInterview;
		vm.uploadImage = uploadImage;
		vm.addUse = addUse;
		vm.removeUse = removeUse;
		vm.editProduct = editProduct;

		ColorService.setCurrentColor("blue");

		function uploadInterview(file) {

			AdminEditProductService.uploadInterview(file, cm.timestamp);
		}

		function uploadImage(file, index) {

			AdminEditProductService.uploadImage(file, cm.timestamp, index);
		}

		function addUse() {

			vm.product.uses.push({ "value": "" });
		}

		function removeUse() {

			vm.product.uses.pop();
		}

		function editProduct(isValid) {

			if (isValid && !LoadingService.isLoading()) {

				vm.formDisabled = true;
				vm.product.subcategories = AdminProductsService.buildSubcategoriesString(vm.product.subcategoriesObject);
				vm.product.formattedUses = AdminProductsService.buildUsesString(vm.product.uses);
				vm.product.imageCount = vm.imageFields.length;
				vm.product.imagesString = AdminProductsService.buildImagesString(vm.product.imageCount, cm.timestamp);
				AdminEditProductService.editProduct(vm.product);

			} else {

				alert("Please complete all required fields and ensure file uploads have completed.");
			}
		}
	}
}(angular));
