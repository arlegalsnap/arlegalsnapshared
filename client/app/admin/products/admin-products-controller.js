(function(ng) {

	"use strict";

	ng.module("SnapApp").controller("AdminProductsController", AdminProductsController);

	AdminProductsController.$inject = ["ColorService", "resolveCategoriesAndProducts"];

	function AdminProductsController(ColorService, resolveCategoriesAndProducts) {

		var vm = this;

		vm.categories = resolveCategoriesAndProducts.categories;
		vm.products = resolveCategoriesAndProducts.products;

		ColorService.setCurrentColor("blue");
	}
}(angular));
