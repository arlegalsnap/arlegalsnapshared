(function(ng) {

	"use strict";

	ng.module("SnapApp").factory("LoginService", LoginService);

	LoginService.$inject = ["$location", "$http"];

	function LoginService($location, $http) {

		var service = {

			login: login,
			getAuthenticationStatus: getAuthenticationStatus
		};

		return service;

		function login(username, password) {

			$http.post("server/admin/login.php", { username: username, password: password }).then(function(response) {

				if (response.data === "success") {

					$location.path("admin/home");
				}
			});
		}

		function getAuthenticationStatus() {

			return $http.get("server/admin/isAuthenticated.php");
		}
	}
}(angular));
