(function(ng) {

	"use strict";

	ng.module("SnapApp").controller("LoginController", LoginController);

	LoginController.$inject = ["ColorService", "LoginService"];

	function LoginController(ColorService, LoginService) {

		var vm = this;

		vm.login = login;

		ColorService.removeColor();

		function login(isValid) {

			if (isValid) {

				LoginService.login(vm.username, vm.password);
			}
		};
	}
}(angular));
