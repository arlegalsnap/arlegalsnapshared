(function(ng) {

	"use strict";

	ng.module("SnapApp").factory("AdminService", AdminService);

	AdminService.$inject = ["$http", "$location", "$q", "LoadingService", "DataService"];

	function AdminService($http, $location, $q, LoadingService, DataService) {

		var service = {

			requireAuthentication: requireAuthentication
		};

		return service;

		function requireAuthentication(limitToEnvironment) {

			var deferred = $q.defer();

			LoadingService.startLoading();
			$http.get("server/admin/isAuthenticated.php").then(function(response) {

				LoadingService.stopLoading();
				if (response.data === "true") {


					DataService.getEnvironment().then(function(response) {

						if (response === limitToEnvironment || !limitToEnvironment) {

							deferred.resolve();

						} else {

							$location.path("/admin/home");
						}
					});

				} else {

					$location.path("/login");
				}
			});

			return deferred.promise;
		}
	}
}(angular));
