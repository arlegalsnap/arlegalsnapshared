(function(ng) {

	"use strict";

	ng.module("SnapApp").controller("AdminHomeController", AdminHomeController);

	AdminHomeController.$inject = ["ColorService", "resolveEnvironment"];

	function AdminHomeController(ColorService, resolveEnvironment) {

		var vm = this;

		vm.environment = resolveEnvironment;

		ColorService.removeColor();
	}
}(angular));
