(function(ng) {

	"use strict";

	ng.module("SnapApp").controller("AdminContentController", AdminContentController);

	AdminContentController.$inject = ["ColorService", "resolveContentPages"];

	function AdminContentController(ColorService, resolveContentPages) {

		var vm = this;

		vm.pages = resolveContentPages;

		ColorService.setCurrentColor("orange");
	}
}(angular));
