(function(ng) {

	"use strict";

	ng.module("SnapApp").factory("AdminEditContentService", AdminEditContentService);

	AdminEditContentService.$inject = ["$http", "$location", "LoadingService", "DataService"];

	function AdminEditContentService($http, $location, LoadingService, DataService) {

		return {

			editContent: function(page) {

				LoadingService.startLoading();
				$http.post("server/admin/editContentPage.php", page).then(function() {

					LoadingService.stopLoading();
					DataService.clearContentPagesCache();
					DataService.clearContentPageDetailsCache(page.pageName);
					$location.path("admin/content");
				});
			}
		};
	}
}(angular));
