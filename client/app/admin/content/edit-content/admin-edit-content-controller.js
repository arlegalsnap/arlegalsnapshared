(function(ng) {

	"use strict";

	ng.module("SnapApp").controller("AdminEditContentController", AdminEditContentController);

	AdminEditContentController.$inject = ["ColorService", "CategoryService", "resolveContentPage", "AdminEditContentService"];

	function AdminEditContentController(ColorService, CategoryService, resolveContentPage, AdminEditContentService) {

		var vm = this;

		vm.page = resolveContentPage;
		vm.editContentForm = editContentForm;

		ColorService.setCurrentColor("orange");
		CategoryService.setCurrentCategoryName("");

		function editContentForm() {

			AdminEditContentService.editContent(vm.page);
		}
	}
}(angular));
