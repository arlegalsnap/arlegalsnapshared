(function(ng) {

    "use strict";

    ng.module("SnapApp").config(AdminRoutes);

    AdminRoutes.$inject = ["$routeProvider"];

    function AdminRoutes($routeProvider) {

		$routeProvider.when("/admin", {

			redirectTo: "/admin/home"

		}).when("/login", {

			templateUrl: "client/app/admin/login/login.html",
			caseInsensitiveMatch: true,
			controller: "LoginController",
			controllerAs: "loginModel"

		}).when("/admin/home", {

			templateUrl: "client/app/admin/home/admin-home.html",
			caseInsensitiveMatch: true,
			controller: "AdminHomeController",
			controllerAs: "adminModel",
			resolve: {

				"auth": ["AdminService", function(AdminService) {

					return AdminService.requireAuthentication();
				}],
				"resolveEnvironment": ["DataService", function(DataService) {

					return DataService.getEnvironment();
				}]
			}
		});
	}
}(angular));
