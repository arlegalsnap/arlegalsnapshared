(function(ng) {

    "use strict";

    ng.module("SnapApp").config(MainRoutes);

    MainRoutes.$inject = ["$routeProvider"];

    function MainRoutes($routeProvider) {

		$routeProvider.when("/", {

			templateUrl: "client/app/pages/home/home.html",
			caseInsensitiveMatch: true,
			controller: "HomeController",
			controllerAs: "homeModel"

		}).when("/content/:page", {

			templateUrl: "client/app/content/content.html",
			caseInsensitiveMatch: true,
			controller: "ContentController",
			controllerAs: "contentModel",
			resolve: {

				"resolvePage": ["DataService", "$route", function(DataService, $route) {

					return DataService.getContentPage($route.current.params.page);
				}]
			}

		}).when("/category/:categoryName", {

			templateUrl: "client/app/pages/category/category.html",
			caseInsensitiveMatch: true,
			controller: "CategoryController",
			controllerAs: "categoryModel",
			resolve: {

				"resolveCategoriesAndProducts": ["DataService", function(DataService) {

					return DataService.getCategoriesAndProducts();
				}]
			}

		}).when("/product/:productName/:productTimestamp", {

			templateUrl: "client/app/pages/product/product.html",
			caseInsensitiveMatch: true,
			controller: "ProductController",
			controllerAs: "productModel",
			resolve: {

				"resolveProduct": ["$route", "DataService", function($route, DataService) {

					return DataService.getProduct($route.current.params.productTimestamp);
				}]
			}

		}).when("/checkout/:productTimestamp", {

			templateUrl: "client/app/pages/checkout/checkout.html",
			caseInsensitiveMatch: true,
			controller: "CheckoutController",
			controllerAs: "checkoutModel",
			resolve: {

				"resolveSetCheckout": ["$route", "DataService", function($route, DataService) {

					return DataService.setCheckout($route.current.params.productTimestamp);
				}],
				"resolveGetAuth": ["LoginService", function(LoginService) {

					return LoginService.getAuthenticationStatus();
				}]
			}

		}).when("/interview", {

			templateUrl: "client/app/pages/interview/interview.html",
			caseInsensitiveMatch: true,
			controller: "InterviewController",
			controllerAs: "interviewModel",
			resolve: {

				"resolveInterview": ["DataService", "LoadingService", function(DataService, LoadingService) {

					LoadingService.startLoading();
					return DataService.loadInterviewData();
				}]
			}

		}).otherwise({

			redirectTo: "/"
		});
	}
}(angular));
