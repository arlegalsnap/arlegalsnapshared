(function(ng) {

    "use strict";

    ng.module("SnapApp").config(AdminImportExportRoutes);

	AdminImportExportRoutes.$inject = ["$routeProvider"];

	function AdminImportExportRoutes($routeProvider) {

		$routeProvider.when("/admin/export", {

			templateUrl: "client/app/admin/export/admin-export.html",
			caseInsensitiveMatch: true,
			controller: "AdminExportController",
			controllerAs: "exportModel",
			resolve: {

				"auth": ["AdminService", function(AdminService) {

					return AdminService.requireAuthentication("local");
				}]
			}

		}).when("/admin/import", {

			templateUrl: "client/app/admin/import/admin-import.html",
			caseInsensitiveMatch: true,
			controller: "AdminImportController",
			controllerAs: "importModel",
			resolve: {

				"auth": ["AdminService", function(AdminService) {

					return AdminService.requireAuthentication("live");
				}]
			}
		});
	}
}(angular));
