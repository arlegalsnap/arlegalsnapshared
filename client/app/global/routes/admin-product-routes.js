(function(ng) {

    "use strict";

    ng.module("SnapApp").config(AdminProductRoutes);

	AdminProductRoutes.$inject = ["$routeProvider"];

	function AdminProductRoutes($routeProvider) {

		$routeProvider.when("/admin/products", {

			templateUrl: "client/app/admin/products/admin-products.html",
			caseInsensitiveMatch: true,
			controller: "AdminProductsController",
			controllerAs: "productModel",
			resolve: {

				"auth": ["AdminService", function(AdminService) {

					return AdminService.requireAuthentication("local");
				}],
				"resolveCategoriesAndProducts": ["DataService", function(DataService) {

					return DataService.getCategoriesAndProducts();
				}]
			}

		}).when("/admin/product/create", {

			templateUrl: "client/app/admin/products/create-product/admin-create-product.html",
			caseInsensitiveMatch: true,
			controller: "AdminCreateProductController",
			controllerAs: "productModel",
			resolve: {

				"auth": ["AdminService", function(AdminService) {

					return AdminService.requireAuthentication("local");
				}]
			}

		}).when("/admin/product/edit/:timestamp", {

			templateUrl: "client/app/admin/products/edit-product/admin-edit-product.html",
			caseInsensitiveMatch: true,
			controller: "AdminEditProductController",
			controllerAs: "productModel",
			resolve: {

				"auth": ["AdminService", function(AdminService) {

					return AdminService.requireAuthentication("local");
				}],
				"resolveCategories": ["DataService", function(DataService) {

					return DataService.getCategories();
				}],
				"resolveProduct": ["$route", "DataService", function($route, DataService) {

					return DataService.getProduct($route.current.params.timestamp);
				}]
			}

		}).when("/admin/product/delete/:timestamp", {

			templateUrl: "client/app/admin/products/delete-product/admin-delete-product.html",
			caseInsensitiveMatch: true,
			controller: "AdminDeleteProductController",
			controllerAs: "productModel",
			resolve: {

				"auth": ["AdminService", function(AdminService) {

					return AdminService.requireAuthentication("local");
				}],
				"resolveProduct": ["$route", "DataService", function($route, DataService) {

					return DataService.getProduct($route.current.params.timestamp);
				}]
			}

		});
	}
}(angular));
