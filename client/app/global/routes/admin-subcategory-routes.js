(function(ng) {

    "use strict";

    ng.module("SnapApp").config(AdminSubcategoryRoutes);

	AdminSubcategoryRoutes.$inject = ["$routeProvider"];

	function AdminSubcategoryRoutes($routeProvider) {

		$routeProvider.when("/admin/subcategories", {
			templateUrl: "client/app/admin/subcategories/admin-subcategories.html",
			caseInsensitiveMatch: true,
			controller: "AdminSubcategoriesController",
			controllerAs: "categoryModel",
			resolve: {

				"auth": ["AdminService", function(AdminService) {

					return AdminService.requireAuthentication("local");
				}],
				"resolveCategories": ["DataService", function(DataService) {

					return DataService.getCategories();
				}]
			}

		}).when("/admin/subcategory/create", {

			templateUrl: "client/app/admin/subcategories/create-subcategory/admin-create-subcategory.html",
			caseInsensitiveMatch: true,
			controller: "AdminCreateSubcategoryController",
			controllerAs: "categoryModel",
			resolve: {

				"auth": ["AdminService", function(AdminService) {

					return AdminService.requireAuthentication("local");
				}],
				"resolveCategories": ["DataService", function(DataService) {

					return DataService.getCategories();
				}]
			}

		}).when("/admin/subcategory/edit/:timestamp", {

			templateUrl: "client/app/admin/subcategories/edit-subcategory/admin-edit-subcategory.html",
			caseInsensitiveMatch: true,
			controller: "AdminEditSubcategoryController",
			controllerAs: "categoryModel",
			resolve: {

				"auth": ["AdminService", function(AdminService) {

					return AdminService.requireAuthentication("local");
				}],
				"resolveCategories": ["DataService", function(DataService) {

					return DataService.getCategories();
				}]
			}

		}).when("/admin/subcategory/delete/:timestamp", {

			templateUrl: "client/app/admin/subcategories/delete-subcategory/admin-delete-subcategory.html",
			caseInsensitiveMatch: true,
			controller: "AdminDeleteSubcategoryController",
			controllerAs: "categoryModel",
			resolve: {

				"auth": ["AdminService", function(AdminService) {

					return AdminService.requireAuthentication("local");
				}],
				"resolveCategories": ["DataService", function(DataService) {

					return DataService.getCategories();
				}]
			}
		});
	}
}(angular));
