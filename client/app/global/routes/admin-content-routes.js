(function(ng) {

    "use strict";

    ng.module("SnapApp").config(AdminContentRoutes);

	AdminContentRoutes.$inject = ["$routeProvider"];

	function AdminContentRoutes($routeProvider) {

		$routeProvider.when("/admin/content", {

			templateUrl: "client/app/admin/content/admin-content.html",
			caseInsensitiveMatch: true,
			controller: "AdminContentController",
			controllerAs: "contentModel",
			resolve: {

				"auth": ["AdminService", function(AdminService) {

					return AdminService.requireAuthentication("local");
				}],
				"resolveContentPages": ["DataService", function(DataService) {

					return DataService.getContentPages();
				}]
			}

		}).when("/admin/content/edit/:page", {

			templateUrl: "client/app/admin/content/edit-content/admin-edit-content.html",
			caseInsensitiveMatch: true,
			controller: "AdminEditContentController",
			controllerAs: "contentModel",
			resolve: {

				"auth": ["AdminService", function(AdminService) {

					return AdminService.requireAuthentication("local");
				}],
				"resolveContentPage": ["$route", "DataService", function($route, DataService) {

					return DataService.getContentPage($route.current.params.page);
				}]
			}
		});
	}
}(angular));
