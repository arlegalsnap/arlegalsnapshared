(function(ng) {

	"use strict";

	ng.module("SnapApp").filter("SafeHTML", SafeHTML);
	ng.module("SnapApp").filter("UrlEncode", UrlEncode);
	ng.module("SnapApp").filter("UrlDecode", UrlDecode);
	ng.module("SnapApp").filter("Trim", Trim);
	ng.module("SnapApp").filter("PriceToFloat", PriceToFloat);

	SafeHTML.$inject = ["$sce"];

	function SafeHTML($sce) {

		return function(HTML) {

			return $sce.trustAsHtml(HTML);
		};
	}

	function UrlEncode() {

		return function(string) {

			var encoded = encodeURI(string);
			return encoded.split("%20").join("-");
		};
	}

	function UrlDecode() {

		return function(string) {

			var dashesConverted = string.split("-").join("%20");
			return decodeURI(dashesConverted);
		};
	}

	function Trim() {

		return function(theString) {

			if (theString.length) return theString.trim();
			return theString;
		};
	}

	function PriceToFloat() {

		return function(string) {

			if (string) {

				return string.split("$").join("").split(" ").join("");
			}
		};
	}
}(angular));
