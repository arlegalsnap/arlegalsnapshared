(function(ng) {

	"use strict";

	ng.module("SnapApp", ["ngRoute", "ngFileUpload"]);
}(angular));
