(function(ng) {

	"use strict";

	ng.module("SnapApp").factory("ColorService", ColorService);

	ColorService.$inject = ["$rootScope"];

	function ColorService($rootScope) {

		var services = {

			currentColor: "",
			removeColor: removeColor,
			setCurrentColor: setCurrentColor,
			broadcast: broadcast
		};

		return services;

		function removeColor() {

			services.currentColor = "";
			broadcast();
		}

		function setCurrentColor(newColor) {

			services.currentColor = newColor;
			broadcast();
		}

		function broadcast() {

			$rootScope.$broadcast("colorUpdate");
		}
	}
}(angular));
