(function(ng) {

	"use strict";

	ng.module("SnapApp").factory("SEOService", SEOService);

	SEOService.$inject = ["$rootScope"];

	function SEOService($rootScope) {

		var service = {

			defaultPageTitle: "Arkansas Legal Snap",
			defaultPageDescription: "",
			pageTitle: this.defaultPageTitle,
			pageDescription: this.defaultPageDescription,
			setPageTitle: setPageTitle,
			resetPageTitle: resetPageTitle,
			setPageDescription: setPageDescription,
			resetPageDescription: resetPageDescription,
			broadcast: broadcast
		};

		return service;

		function setPageTitle(newTitle) {

			service.pageTitle = newTitle;
			broadcast();
		}

		function resetPageTitle() {

			service.pageTitle = service.defaultPageTitle;
			broadcast();
		}

		function setPageDescription(newDescription) {

			service.pageDescription = newDescription;
			broadcast();
		}

		function resetPageDescription() {

			service.pageDescription = service.defaultPageDescription;
			broadcast();
		}

		function broadcast() {

			$rootScope.$broadcast("seoUpdate");
		}
	}
}(angular));
