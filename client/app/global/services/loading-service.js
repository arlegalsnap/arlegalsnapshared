(function(ng) {

	"use strict";

	ng.module("SnapApp").factory("LoadingService", LoadingService);

	LoadingService.$inject = ["$rootScope"];

	function LoadingService($rootScope) {

		var service = {

			loadingStatus: false,
			isLoading: isLoading,
			startLoading: startLoading,
			stopLoading: stopLoading,
			broadcast: broadcast
		};

		return service;

		function isLoading() {

			return service.loadingStatus;
		}

		function startLoading() {

			service.loadingStatus = true;
			broadcast();
		}

		function stopLoading() {

			service.loadingStatus = false;
			broadcast();
		}

		function broadcast() {

			$rootScope.$broadcast("loadingStatusChanged");
		}
	}
}(angular));
