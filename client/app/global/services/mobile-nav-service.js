(function(ng) {

	"use strict";

	ng.module("SnapApp").factory("MobileNavService", MobileNavService);

	function MobileNavService() {

		var service = {

			mobileNavOpen: false,
			toggleMobileNav: toggleMobileNav,
			showMobileNav: showMobileNav,
			hideMobileNav: hideMobileNav
		};

		return service;

		function toggleMobileNav($scope) {

			if (service.mobileNavOpen) {

				service.mobileNavOpen = false;
				hideMobileNav($scope);

			} else {

				service.mobileNavOpen = true;
				showMobileNav($scope);
			}
		}

		function showMobileNav($scope) {

			$scope.mobileNavOpen = "mobile-nav-open";
		}

		function hideMobileNav($scope) {

			$scope.mobileNavOpen = null;
		}
	}
}(angular));
