(function(ng) {

	"use strict";

	ng.module("SnapApp").factory("DataService", DataService);

	DataService.$inject = ["$http", "$q", "$location", "LoadingService"];

	function DataService($http, $q, $location, LoadingService) {

		var service = {

			// Data containers
			categories: null,
			products: null,
			productDetails: {},
			contentPages: null,
			contentPageDetails: {},
			environment: null,
			// Get/Post actions
			getCategories: getCategories,
			getProducts: getProducts,
			getProduct: getProduct,
			getCategoriesAndProducts: getCategoriesAndProducts,
			getContentPages: getContentPages,
			getContentPage: getContentPage,
			setCheckout: setCheckout,
			processPayment: processPayment,
			loadInterviewData: loadInterviewData,
			getEnvironment: getEnvironment,
			// Cache management
			clearCategoriesCache: clearCategoriesCache,
			clearProductsCache: clearProductsCache,
			clearProductDetailsCache: clearProductDetailsCache,
			clearContentPagesCache: clearContentPagesCache,
			clearContentPageDetailsCache: clearContentPageDetailsCache
		};

		return service;

		function getCategories() {

			var deferred = $q.defer();

			if (service.categories) {

				deferred.resolve(service.categories);

			} else {

				LoadingService.startLoading();
				$http.get("server/api/getCategories.php").then(function(response) {

					LoadingService.stopLoading();
					service.categories = response.data;
					deferred.resolve(service.categories);
				});
			}

			return deferred.promise;
		}

		function getProducts() {

			var deferred = $q.defer();

			if (service.products) {

				deferred.resolve(service.products);

			} else {

				LoadingService.startLoading();
				$http.get("server/api/getProducts.php").then(function(response) {

					LoadingService.stopLoading();
					service.products = response.data;
					deferred.resolve(service.products);
				});
			}

			return deferred.promise;
		}

		function getProduct(timestamp) {

			var deferred = $q.defer();

			if (service.productDetails[timestamp]) {

				deferred.resolve(service.productDetails[timestamp]);

			} else {

				LoadingService.startLoading();
				$http.get("server/api/getProduct.php?timestamp=" + timestamp).then(function(response) {

					LoadingService.stopLoading();
					service.productDetails[timestamp] = response.data[0];
					deferred.resolve(response.data[0]);
				});
			}

			return deferred.promise;
		}

		function getCategoriesAndProducts() {

			// Allows loading both in tandem instead of queueing
			var data = { "categories": null, "products": null },
				deferred = $q.defer();

			if (service.categories) {

				data.categories = service.categories;

			} else {

				LoadingService.startLoading();
				$http.get("server/api/getCategories.php").then(function(response) {

					service.categories = response.data;
					data.categories = service.categories;
					if (data.products) finish();
				});
			}

			if (service.products) {

				data.products = service.products;
				if (data.categories) finish();

			} else {

				$http.get("server/api/getProducts.php").then(function(response) {

					service.products = response.data;
					data.products = service.products;
					if (data.categories) finish();
				});
			}

			function finish() {

				LoadingService.stopLoading();
				deferred.resolve(data);
			}

			return deferred.promise;
		}

		function  getContentPages() {

			var deferred = $q.defer();

			if (service.contentPages) {

				deferred.resolve(service.contentPages);

			} else {

				LoadingService.startLoading();
				$http.get("server/api/getContentPages.php").then(function(response) {

					LoadingService.stopLoading();
					service.contentPages = response.data;
					deferred.resolve(service.contentPages);
				});
			}

			return deferred.promise;
		}

		function getContentPage(pageName) {

			var deferred = $q.defer();

			if (service.contentPageDetails[pageName]) {

				deferred.resolve(service.contentPageDetails[pageName]);

			} else {

				LoadingService.startLoading();
				$http.get("server/api/getContentPage.php?pageName=" + pageName).then(function(response) {

					LoadingService.stopLoading();
					if (response.data[0]) {

						service.contentPageDetails[pageName] = response.data[0];
						deferred.resolve(response.data[0]);

					} else {

						$location.path("/");
					}
				});
			}

			return deferred.promise;
		}

		function setCheckout(timestamp) {

			var deferred = $q.defer();
			LoadingService.startLoading();
			$http.get("server/api/setCheckout.php?timestamp=" + timestamp).then(function(response) {

				LoadingService.stopLoading();
				deferred.resolve(response.data);
			});
			return deferred.promise;
		}

		function processPayment(formObject) {

			var deferred = $q.defer();
			LoadingService.startLoading();
			$http.post("server/api/processPayment.php", formObject).then(function(response) {

				LoadingService.stopLoading();
				deferred.resolve(response);
			});
			return deferred.promise;
		}

		function loadInterviewData() {

			// LoadingService handled in controller here since there's an iframe to wait on
			var deferred = $q.defer();
			$http.get("server/api/loadInterviewData.php").then(function(response) {

				deferred.resolve(response.data);
			});
			return deferred.promise;
		}

		function getEnvironment() {

			var deferred = $q.defer();

			if (service.environment) {

				deferred.resolve(service.environment);

			} else {

				$http.get("server/admin/getEnvironment.php").then(function(response) {

					service.environment = response.data;
					deferred.resolve(service.environment);
				});
			}

			return deferred.promise;
		}

		function clearCategoriesCache() {

			service.categories = null;
		}

		function clearProductsCache() {

			service.products = null;
		}

		function clearProductDetailsCache(timestamp) {

			delete service.productDetails[timestamp];
		}

		function clearContentPagesCache() {

			service.contentPages = null;
		}

		function clearContentPageDetailsCache(pageName) {

			delete service.contentPageDetails[pageName];
		}
	}
}(angular));
