(function(ng) {

	"use strict";

	ng.module("SnapApp").controller("GlobalController", GlobalController);

	GlobalController.$inject = ["$scope", "$rootScope", "LoadingService", "LoginService", "DataService", "MobileNavService", "SEOService", "ColorService", "CategoryService"];

	function GlobalController($scope, $rootScope, LoadingService, LoginService, DataService, MobileNavService, SEOService, ColorService, CategoryService) {

		// Environment flag
		DataService.getEnvironment().then(function(response) {

			$scope.isLocal = (response == "local");
		});

		// Admin flag
		$scope.$on("$routeChangeSuccess", refreshAdminStatus);
		refreshAdminStatus();

		// Loading indicator
		$rootScope.loading = LoadingService.isLoading();
		$scope.$on("loadingStatusChanged", function() {

			$rootScope.loading = LoadingService.isLoading();
		});

		// Mobile nav
		$rootScope.toggleMobileNav = function() { MobileNavService.toggleMobileNav($rootScope); };
		$rootScope.showMobileNav = function() { MobileNavService.showMobileNav($rootScope); };
		$rootScope.hideMobileNav = function() { MobileNavService.hideMobileNav($rootScope); };
		$rootScope.mobileNavOpen = MobileNavService.mobileNavOpen;

		// SEO -- Page title and description meta
		function setSEOValues() {

			$rootScope.pageTitle = SEOService.pageTitle;
			$rootScope.pageDescription = SEOService.pageDescription;
		}
		setSEOValues();
		$scope.$on("$routeChangeSuccess", function() {

			SEOService.resetPageTitle();
			SEOService.resetPageDescription();
		});
		$scope.$on("seoUpdate", setSEOValues);

		DataService.getCategories().then(function(response) {

			$rootScope.categories = response;
		});

		// Category styling management
		$rootScope.currentCategory = CategoryService.getCurrentCategoryName();
		$scope.$on("categoryNameUpdate", function() {

			$rootScope.currentCategory = CategoryService.getCurrentCategoryName();
		});

		// Color scheme management
		function updateColor() {

			$rootScope.currentColor = ColorService.currentColor;
		}
		updateColor();
		$scope.$on("colorUpdate", updateColor);

		// Nick nacks
		$rootScope.interviewLoadedThisSession = false;
		$rootScope.goBack = function() { window.history.back(); };

		function refreshAdminStatus() {

			LoginService.getAuthenticationStatus().then(function(response) {

				$scope.isAdmin = (response.data == "true");
			});
		}
	}
}(angular));
