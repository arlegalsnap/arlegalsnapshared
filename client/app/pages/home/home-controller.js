(function(ng) {

	"use strict";

	ng.module("SnapApp").controller("HomeController", HomeController);

	HomeController.$inject = ["ColorService", "CategoryService"];

	function HomeController(ColorService, CategoryService) {

		ColorService.removeColor();
		CategoryService.setCurrentCategoryName("");
	}
}(angular));
