(function(ng) {

	"use strict";

	ng.module("SnapApp").controller("InterviewController", InterviewController);

	InterviewController.$inject = ["$scope", "$rootScope", "resolveInterview", "LoadingService"];

	function InterviewController($scope, $rootScope, resolveInterview, LoadingService) {

		var vm = this,
			cm = {};

		cm.timeoutHandle;
		cm.hdSessionID;
		cm.interviewResponse = resolveInterview;

		vm.showError = false;
		vm.product;
		vm.interviewHTML;

		reloadIfNeeded();

		$rootScope.interviewLoadedThisSession = true;
		$rootScope.focusMode = "focus-mode";
		$scope.$on("$locationChangeStart", browseAway);

		errorOrLoadCheck();

		function reloadIfNeeded() {

			// Prevents issue of iframe not loading when browsing away and back to this page
			if ($rootScope.interviewLoadedThisSession) {

				window.location.reload();
			}
		}

		function errorOrLoadCheck() {

			if (!cm.interviewResponse.hdSessionID) {

				vm.showError = true;
				LoadingService.stopLoading();

			} else {

				instantiateInterview();
			}
		}

		function instantiateInterview() {

			cm.hdSessionID = cm.interviewResponse.hdSessionID;
			vm.product = cm.interviewResponse.product[0];
			vm.interviewHTML = cm.interviewResponse.interviewHTML;

			(function checkFrame() {

				var interviewContainer = document.getElementById("interview-container");

				if (interviewContainer) {

					LoadingService.stopLoading();
					$rootScope.$apply();
					HD$.CreateInterviewFrame("interview-container", cm.hdSessionID);

				} else {

					cm.timeoutHandle = setTimeout(checkFrame, 1000);
				}
			}());
		}

		function browseAway() {

			$rootScope.focusMode = null;
			clearTimeout(cm.timeoutHandle);
			LoadingService.stopLoading();
		}
	}
}(angular));
