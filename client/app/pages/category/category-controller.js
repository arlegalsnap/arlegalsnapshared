(function(ng) {

	"use strict";

	ng.module("SnapApp").controller("CategoryController", CategoryController);

	CategoryController.$inject = ["$routeParams", "$filter", "CategoryService", "ColorService", "resolveCategoriesAndProducts"];

	function CategoryController($routeParams, $filter, CategoryService, ColorService, resolveCategoriesAndProducts) {

		var vm = this,
			cm = {};

		cm.resolveData = resolveCategoriesAndProducts;
		cm.categories = cm.resolveData.categories;
		cm.categoryName = $filter("UrlDecode")($routeParams.categoryName);

		vm.category = CategoryService.getCategoryFromCategories(cm.categoryName, cm.categories);
		vm.products = CategoryService.filterProductsByCategoryName(vm.category.name, cm.resolveData.products, cm.categories);
		vm.previewImages = CategoryService.createPreviewImagesArray(vm.products);
		vm.heroProducts = CategoryService.buildHeroProducts(vm.products, vm.category.subcategories);

		ColorService.setCurrentColor(CategoryService.getColorForCategory(vm.category.name, cm.categories));
		CategoryService.setCurrentCategoryName(vm.category.name);
	}
}(angular));
