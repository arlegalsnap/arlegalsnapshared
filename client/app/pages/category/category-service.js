(function(ng) {

	"use strict";

	ng.module("SnapApp").factory("CategoryService", CategoryService);

	CategoryService.$inject = ["$rootScope"];

	function CategoryService($rootScope) {

		var service = {

			currentCategoryName: "",
			getCurrentCategoryName: getCurrentCategoryName,
			setCurrentCategoryName: setCurrentCategoryName,
			buildHeroProducts: buildHeroProducts,
			createPreviewImagesArray: createPreviewImagesArray,
			getColorForCategory: getColorForCategory,
			getCategoryFromCategories: getCategoryFromCategories,
			getCategoryNameForSubcategoryTimestamp: getCategoryNameForSubcategoryTimestamp,
			filterProductsByCategoryName: filterProductsByCategoryName,
			getSubcategoryNameGivenSubcategoryTimestamp: getSubcategoryNameGivenSubcategoryTimestamp,

		};

		return service;

		function getCurrentCategoryName() {

			return service.currentCategoryName;
		}

		function setCurrentCategoryName(newName) {

			service.currentCategoryName = newName;
			$rootScope.$broadcast("categoryNameUpdate");
		}

		// Make array of products. Rules are 1) maximum of 3 products, maximum of one product per subcategory
		function buildHeroProducts(scopedProducts, subcategories) {

			var heroProducts = [];

			ng.forEach(subcategories, function(thisSubcategory) {

				var subcategoryTimestamp = thisSubcategory.timestamp,
					newProduct = {};

				ng.forEach(scopedProducts, function(thisProduct) {

					var alreadyProductInThisSubcategory = !!newProduct.name,
						productFoundInSubcategory = thisProduct.subcategories.indexOf(subcategoryTimestamp) > -1,
						maxHeroProductsMet = (heroProducts.length === 3),
						productAlreadyInList = (heroProducts.indexOf(thisProduct) > -1);

					if (productFoundInSubcategory && !maxHeroProductsMet && !alreadyProductInThisSubcategory && !productAlreadyInList) {

						newProduct = thisProduct;
						heroProducts.push(newProduct);
					}
				});
			});

			return heroProducts;
		}

		// Get all (or up to 10) images from products on this page (only 1st image of each product)
		function createPreviewImagesArray(products) {

			var imagesArray = [],
				maxCount = Math.min(10, products.length),
				x;

			for (x = 0; x < maxCount; x++) {

				if (products[x].images[0].length) imagesArray.push(products[x].images[0]);
			}

			return imagesArray;
		}

		// Given the category name, return its correspondin color from the entire categories data collection
		function getColorForCategory(categoryName, categoriesData) {

			var color = "";

			ng.forEach(categoriesData, function(thisCategory) {

				if (categoryName == thisCategory.name) color = thisCategory.color;
			});

			return color;
		}

		// Return full category data given category name
		function getCategoryFromCategories(categoryName, categoriesData) {

			var category = null;

			ng.forEach(categoriesData, function(thisCategory) {

				if (categoryName === thisCategory.name) category = thisCategory;
			});

			return category;
		}

		// Return name of category given timestamp value of a subcategory
		function getCategoryNameForSubcategoryTimestamp(subcategoryTimestamp, categoriesData) {

			var categoryName = null;

			ng.forEach(categoriesData, function(thisCategory) {

				ng.forEach(thisCategory.subcategories, function(thisSubcategory) {

					if (subcategoryTimestamp === thisSubcategory.timestamp) categoryName = thisCategory.name;
				});
			});

			return categoryName;
		}

		// Using full categories and products data sets, return products matching given category name
		function filterProductsByCategoryName(categoryName, products, categoriesData) {

			var matchingProducts = [],
				thisCategorySubcategories;

			// Get subcategories array from current category
			ng.forEach(categoriesData, function(thisCategory) {

				if (categoryName == thisCategory.name) thisCategorySubcategories = thisCategory.subcategories;
			});

			// Populate matching products (from complete products collection)
			ng.forEach(products, function(thisProduct) {

				ng.forEach(thisCategorySubcategories, function(thisSubcategory) {

					var productSubcats = thisProduct.subcategories,
						thisSubcatTimestamp = thisSubcategory.timestamp,
						productContainsThisSubcategory = (productSubcats.indexOf(thisSubcatTimestamp) > -1),
						productAlreadyInList = (matchingProducts.indexOf(thisProduct) > -1);

					if (productContainsThisSubcategory && !productAlreadyInList) matchingProducts.push(thisProduct);
				});
			});

			return matchingProducts;
		}

		function getSubcategoryNameGivenSubcategoryTimestamp(subcategoryTimestamp, categoriesData) {

			var subcategoryName = "";

			ng.forEach(categoriesData, function(thisCategory) {

				if (!subcategoryName) {

					ng.forEach(thisCategory.subcategories, function(thisSubcategory) {

						if (!subcategoryName && subcategoryTimestamp == thisSubcategory.timestamp) {

							subcategoryName = thisSubcategory.name;
						}
					});
				}
			});

			return subcategoryName;
		}
	}
}(angular));
