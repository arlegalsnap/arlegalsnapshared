(function(ng) {

	"use strict";

	ng.module("SnapApp").controller("CheckoutController", CheckoutController);

	CheckoutController.$inject = ["CheckoutService", "resolveSetCheckout", "resolveGetAuth"];

	function CheckoutController(CheckoutService, resolveSetCheckout, resolveGetAuth) {

		var vm = this;

		vm.formDisabled = false;
		vm.isAuthenticated = resolveGetAuth.data;
		vm.formData = {};
		vm.checkout = resolveSetCheckout;
		vm.getStripeToken = getStripeToken;

		function getStripeToken($event, isValid) {

			CheckoutService.saveFormData(vm.formData);
			CheckoutService.getStripeToken(vm, $event, isValid);
		}
	}
}(angular));
