(function(ng) {

	"use strict";

	ng.module("SnapApp").factory("CheckoutService", CheckoutService);

	CheckoutService.$inject = ["$timeout", "$location", "DataService"];

	function CheckoutService($timeout, $location, DataService) {

		var scope,
			formData,
			service = {

			scope: null,
			formData: null,
			saveFormData: saveFormData,
			getStripeToken: getStripeToken,

		};

		return service;

		function saveFormData(data) {

			formData = data;
		}

		function getStripeToken($scope, event, isValid) {

			scope = $scope;

			if (!isValid) {

				alert("Please complete all required fields before continuing.");

			} else {

				var formObject = event.target;
				$timeout(function() { scope.formDisabled = true; });
				Stripe.setPublishableKey('pk_test_tsoh0VwlQA2C8bfpdiMbWQOF');
				Stripe.card.createToken(formObject, stripeResponseHandler);
			}
		}

		function stripeResponseHandler(status, response) {

			if (status !== 200) {

				alert(response.error.message);
				$timeout(function() { scope.formDisabled = false; });

			} else {

				formData.token = response.id;
				DataService.processPayment(formData).then(function(response) {

					if (response.data.success) {

						$location.path("/interview");

					} else {

						alert("There was an error. You have not been charged. Please try again.");
						$timeout(function() { scope.formDisabled = false; });
					}
				});
			}
		}
	}
}(angular));
