(function(ng) {

	"use strict";

	ng.module("SnapApp").factory("ProductService", ProductService);

	ProductService.$inject = ["DataService", "ColorService", "CategoryService"];

	function ProductService(DataService, ColorService, CategoryService) {

		return {

			setColorForProduct: function(productData) {

				if (!ColorService.currentColor) {

					DataService.getCategories().then(function(response) {

						var product = productData,
							categories = response,
							productFirstSubcategoryTimestamp = product.subcategories[0],
							categoryName = CategoryService.getCategoryNameForSubcategoryTimestamp(productFirstSubcategoryTimestamp, categories);

						ColorService.setCurrentColor(CategoryService.getColorForCategory(categoryName, categories));
						CategoryService.setCurrentCategoryName(categoryName);
					});
				}
			}
		};
	}
}(angular));
