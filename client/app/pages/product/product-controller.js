(function(ng) {

	"use strict";

	ng.module("SnapApp").controller("ProductController", ProductController);

	ProductController.$inject = ["SEOService", "ProductService", "resolveProduct"];

	function ProductController(SEOService, ProductService, resolveProduct) {

		var vm = this;
		vm.product = resolveProduct;

		SEOService.setPageTitle(vm.product.name + " - Affordable Arkansas Forms");
		SEOService.setPageDescription(vm.product.productpagedescription);

		ProductService.setColorForProduct(vm.product);
	}
}(angular));
