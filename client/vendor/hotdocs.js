if (typeof HDInterviewOptions != 'object')
	HDInterviewOptions = {};

/*
HD$ facilitates embedding an iframe interview in a web page.
*/
HD$ = {};
(function ($) {
	$._namespace = function() {
		/// <summary>
		/// The Embedded JavaScript API is for use by integrators using HotDocs Embedded to place a HotDocs browser
		/// interview inside an IFRAME on the host web page. It lets you access to the full power of the HotDocs JavaScript
		/// API for customizing browser interviews, using the EasyXDM library for asynchronous communication
		/// with the interview (and vice veras) in a cross-domain-safe manner.
		/// </summary>
		/// <remarks>
		/// Note: In our usage of the EasyXDM library, each callback serves one of two purposes -- it is either an
		/// event handler or it is a continuation callback for an asynchronous call.  In order to avoid confusion,
		/// we use the term "handler" for the former and "callback" for the latter.
		/// </remarks>
	};
	$._class = function() {
		/// <summary>
		/// The HD$ object exposes methods for creating, communicating with and controlling the IFRAME element
		/// in which a HotDocs Embedded interview runs. It encapsulates most of the functionality of the traditional
		/// HotDocs JavaScript API, but adds the facility for asynchronous call-backs as is necessary for
		/// cross-frame messaging.
		/// </summary>
	}
	$._properties = {
		// ********** Public fields **********
		CloudServicesAddress: function () {
			/// <summary>
			/// [string] The URL of the installation of HotDocs Cloud Services to which this instance is communicating.
			/// </summary>
			/// <remarks>
			/// If you are using HotDocs Embedded with an installation besides the default one, you will need to set this property
			/// to the correct alternate address.
			/// </remarks>
			$.CloudServicesAddress = null;
		}
	};
	$._properties.CloudServicesAddress();

	// ********** Private fields **********
	var _container = null;
	var _iframeID = null;
	var _sessionID = null;
	var _rpc = null;
	var _onFrameReady = null;
	var _onInit;
	var _eventHandlers = {};
	var _funcActives = {};
	var _funcEnableds = {};

	// ********** Public functions **********

	$.CreateInterviewFrame = function(containerID, sessionID, onFrameReady) {
		/// <summary>
		/// Create an iframe element and show the requested interview in it.
		/// </summary>
		/// <param name="containerID">[string]The ID of the div element in which the iframe will be created.</param>
		/// <param name="sessionID">[string]The session ID that was returned from the CreateSession REST call.</param>
		/// <param name="onFrameReady">[function]Optional. The event handler to be called after the iframe is created and before the interview is generated.</param>

		if (typeof containerID == 'string') _container = document.getElementById(containerID);
		if (typeof sessionID == 'string') {
			_sessionID = sessionID;
			if ($.CloudServicesAddress == null) {
				if (endsWith(sessionID, 'eu'))
					$.CloudServicesAddress = "https://europe.hotdocs.ws";
				else
					$.CloudServicesAddress = "https://cloud.hotdocs.ws";
			}
		}
		if (typeof onFrameReady == 'function') _onFrameReady = onFrameReady;
		doCreateInterviewFrame(sessionID);
	};

	$.RegisterHandler = function(eventName, handler, callback) {
		/// <summary>
		///   This function is used to register event handlers for various interview events. 
		///   They can be used for both JavaScript and Silverlight interviews. If necessary the event handler  
		///   can later be unregistered using the <see cref="HD_0024.UnregisterHandler"/> method.
		/// </summary>
		/// <param name="eventName">[string]A string representing the name of an interview event. You can specify any string,
		///   but only those listed below (see Remarks) are supported and will cause your event handler to be called.
		/// </param>
		/// <param name="handler">[function]A JavaScript function that will be called any time the event for which it is registered occurs.
		///   It should accept a single parameter, EventArgs, the contents of which vary depending on the event as described below. 
		/// </param>
		/// <param name="callback">[function]A function to call after the handler is registered.</param>
		/// <remarks>
		///   RegisterHandler can be used to register for the following interview events:
		///   <list type="bullet">
		///     <item><description>
		///       "<b>OnHDStart</b>" - This event fires when an interview is fully loaded and initialized in the browser,
		///       but before the user has a chance to interact with the interview. For example, if you want to dictate that the user
		///       should begin the interview at any dialog besides the first one, you could register for this event and call the
		///       HDGotoInterviewBookmark function to set an alternate starting point for the user's interview.
		///       <p>The EventArgs object passed to your event handler does not have any properties.</p>
		///     </description></item>
		///     <item><description>
		///       "<b>OnHDNavigated</b>" - This event fires when the user navigates to a new dialog, including
		///       the dialog first displayed when starting the interview. It is fired after the dialog script has run for the
		///       new dialog, and after the display has been updated. (In the case of the first dialog, it is fired <i>after</i> the
		///       OnHDStart event.)
		///       <p>If you make changes to answers in your handler, you should call HDScreenRefresh as usual to make sure the
		///         screen gets refreshed afterwards.</p>
		///       <p>The EventArgs object passed to your event handler has the following properties:
		///         <include file="../JavaScriptAPI/jsapi.xml" path="Help/EventArgs/Navigated/*"/>
		///       </p>
		///     </description></item>
		///     <item><description>
		///       "<b>PreHDNavigate</b>" - This event fires at the time the user attempts to navigate away
		///       from a dialog. It fires after any pending answer changes have been saved, but before focus has actually
		///       moved away from the current dialog.
		///       <p>The EventArgs object passed to your event handler has the following properties:
		///         <include file="../JavaScriptAPI/jsapi.xml" path="Help/EventArgs/PreNavigate/*"/>
		///       </p>
		///     </description></item>
		///     <item><description>
		///       "<b>OnHDFinish</b>" - This event fires when the user clicks the Finish button, after HotDocs
		///       first ensures that all required variables have been answered. You can use this function to perform custom
		///       answer validation and potentially cancel the user's action if needed.
		///       <p>The EventArgs object when this event is called has the following properties:
		///         <include file="../JavaScriptAPI/jsapi.xml" path="Help/EventArgs/Finish/*"/>
		///       </p>
		///     </description></item>
		///     <item><description>
		///       "<b>OnHDProgress</b>" - This event fires every time a variable is answered (or cleared)
		///       or the interview content otherwise changes, such that information about the user's progress towards completion
		///       of the interview has changed. You might use this event if you have suppressed display of the built-in
		///       interview progress bar (using the HDShowProgressBar variable) and wish to track user progress through some
		///       alternate means.
		///       <p>The EventArgs object when this event is called has the following properties:
		///         <include file="../JavaScriptAPI/jsapi.xml" path="Help/EventArgs/Progress/*"/>
		///       </p>
		///     </description></item>
		///     <item><description>
		///       "<b>OnDocumentsAssembled</b>" - This event fires after a document or series of documents has been assembled, and
		///       before the next interview, if any, is presented.
		///       <p>The EventArgs object when this event is called has the following properties:
		///         <include file="../JavaScriptAPI/jsapi.xml" path="Help/EventArgs/DocumentsAssembled/*"/>
		///       </p>
		///     </description></item>
		///     <item><description>
		///       "<b>OnSessionCompleted</b>" - This event fires after all interviews have been completed and all documents have
		///       been assembled.
        ///       <p>The EventArgs object when this event is called has the following properties:
		///         <include file="../JavaScriptAPI/jsapi.xml" path="Help/EventArgs/SessionCompleted/*"/>
		///       </p>
		///     </description></item>
		///     <item><description>
		///       "<b>OnSaveAnswers</b>" - This event fires when the Save Answers button is clicked.  This button is automatically
		///       added to the toolbar when a handler is registered for the OnSaveAnswers event.
		///       <p>The EventArgs object when this event is called has the following properties:
		///         <include file="../JavaScriptAPI/jsapi.xml" path="Help/EventArgs/SaveAnswers/*"/>
		///       </p>
		///     </description></item>
		///   </list>
		/// </remarks>

		if (!_eventHandlers[eventName]) { // If no handler list exists for this event,
			_eventHandlers[eventName] = [handler]; // create it
			_rpc.RegisterHandler(eventName, callback); // and inform the iframe
		}
		else {
			if (indexOf(_eventHandlers[eventName], handler) === -1) {
				_eventHandlers[eventName].push(handler);
			}
			if (callback)
				callback();
		}
	};

	$.UnregisterHandler = function(eventName, handler, callback) {
		/// <summary>
		/// This function is used to unregister an event handler for an interview event that was registered using the 
		/// <see cref="HD_0024.RegisterHandler"/> method. 
		/// </summary>
		/// <param name="eventName">[string] A string representing the name of an interview event.
		/// You should specify the same string you used when registering the event handler.</param>
		/// <param name="handler">[function] The JavaScript function that was registered as an event handler,
		/// which you would like to unregister.</param>
		/// <param name="callback">[function]A function to call after the handler is unregistered.</param>

		if (_eventHandlers[eventName]) {
			var index = indexOf(_eventHandlers[eventName], handler);
			if (index != -1) { // If the handler is in the list
				_eventHandlers[eventName].splice(index, 1); // Remove from the list
				if (_eventHandlers[eventName] < 1) { // If there are no more handlers for this event,
					delete _eventHandlers[eventName]; // then delete the event from the map
					_rpc.UnregisterHandler(eventName, callback); // and unregister the event
				}
				else
				{
					if (callback)
						callback();
				}
			}
		}
	};

	$.AddCustomNavButton = function(funcClick, funcEnabled, buttonType,
		buttonText, tooltip, imageName, navGroup, buttonIndex, separator, callback) {
		/// <summary>
		/// This function adds a button to the interview navigation bar. When a user clicks the button, a JavaScript function 
		/// is called that performs any action users may need to take while navigating through the interview. 
		/// </summary>
		/// <remarks>
		/// To use this function, place a call to <see cref="AddCustomNavButton"/> inside of your <see cref="HDInterviewOptions.OnInit">HDInterviewOptions.OnInit</see> method.
		/// </remarks>
		/// <param name="funcClick">[function] A Javascript function that should be called whenever the button is clicked.</param>
		/// <param name="funcEnabled">[function] A Javascript function that should be called to determine if the button is enabled or not. It should accept one parameter, which is the ID of the custom navigation button being checked. (Default: null, which means the button will always be enabled)</param>
		/// <param name="buttonType">[number] The type of button to create:
		/// <ul><li>0=TextOnly</li><li>1=ImageOnly</li><li>2=ImageLeft (image to the left of the text)</li><li>3=ImageRight</li></ul></param>
		/// <param name="buttonText">[string] The text (if any) that should be displayed on the button.</param>
		/// <param name="tooltip">[string] The tooltip text for the button.</param>
		/// <param name="imageName">[string] The image (if any) for the button.</param>
		/// <param name="navGroup">[number] Indicates which navigation bar group the new button will be added to: 
		/// <ul><li>0=Left</li><li>1=Middle</li><li>2=Right</li></ul></param>
		/// <param name="buttonIndex">[number] The location within the navigation bar group where the new button will appear. If this is null, the button will just be added to the end of the group.</param>
		/// <param name="separator">[number] Indicates whether or not a separator bar will be placed next to the new button:
		/// <ul><li>0=None</li><li>1=Left</li><li>2=Right</li><li>3=Both</li></ul></param>
		/// <param name="callback">[function] A function to which a button ID for the custom button will be passed. You can later use this button ID invoke the button programmatically or to set the button's visibility.</param>

		_rpc.AddCustomNavButton(typeof funcEnabled == 'function', buttonType,
			buttonText, tooltip, imageName, navGroup, buttonIndex, separator,
			function(buttonID) {
				_eventHandlers[buttonID] = funcClick;
				if (typeof funcEnabled == 'function')
					_funcEnableds[buttonID] = funcEnabled;
				if (callback)
					callback(buttonID);
			}
		);
	};

	$.AddCustomToolbarButton = function(funcClick, imageNormal, imageHot,
		statusText, tooltip, funcActive, imageActive, imageActiveHot,
		tooltipActive, buttonIndex, textOnlyCaption, callback) {
		/// <summary>
		/// This function adds a button to the interview toolbar. When a user clicks the button, 
		/// a JavaScript function is called that performs any action you may wish to make available 
		/// to users of your host application, such as displaying your host application's help file.
		/// (To avoid ambiguity, you should use a full URL for all image file parameters passed to this function.)
		/// </summary>
		/// <remarks>
		/// <p>To use this function, place a call to AddCustomToolbarButton inside of your <see cref="HDInterviewOptions.OnInit">HDInterviewOptions.OnInit</see> method.</p>
		/// <p>The first two parameters are required; the rest are optional.</p>
		/// <p class="Note">Custom buttons are always added to the left of the existing toolbar buttons in the order in which they are added.</p>
		/// <p class="Tip">You can also add custom buttons to the toolbar on the server side using the COM or .NET API, although buttons added on the server side cannot have active and inactive states. (They are always active.)</p>
		/// <p class="Caution">Silverlight interviews support only .PNG and .JPG images. If you intend to use the Silverlight interview format, make sure to use a supported image type.</p>
		/// </remarks>
		/// <param name="funcClick">[function] A Javascript function that should be called whenever the button is clicked. It should accept one parameter, which is the ID for the toolbar button being clicked.</param>
		/// <param name="imageNormal">[string] This is the URL for the button's "normal" image.</param>
		/// <param name="imageHot">[string] This is the URL for the image displayed when the mouse is hovered over the button.</param>
		/// <param name="statusText">[string] This is the text that is displayed in the status bar when the mouse is hovered over the button.</param>
		/// <param name="tooltip">[string] This is the text that is displayed as a hint when the mouse is hovered over the button.</param>
		/// <param name="funcActive">[function] A Javascript function that should be called to determine if the button is active or not. It should accept one parameter, which is the ID for the toolbar button being checked. </param>
		/// <param name="imageActive">[string] This is the image for the button when it is active.</param>
		/// <param name="imageActiveHot">[string] This is the image for the button when it is active and the mouse is hovered over it.</param>
		/// <param name="tooltipActive">[string] This is the text that is displayed as a hint when the button is active and the mouse is hovered over it.</param>
		/// <param name="buttonIndex">[number] This is the location of the button within the group of toolbar buttons. (Default: 0, which means the button will be placed in the first (left-most) position in the group)</param>
		/// <param name="textOnlyCaption">[string] The caption to display on a text-only button.</param>
		/// <param name="callback">[function] A function to which a button ID for the custom button will be passed. You can later use this button ID invoke the button programmatically or to set the button's visibility.</param>
		/// <example>
		/// <code lang="js">
		/// var MyButtonIsActive = true;
		///
		/// function ActiveButton()
		/// {
		/// 	return MyButtonIsActive;
		/// }
		///
		/// function HelloWorld()
		/// {
		/// 	MyButtonIsActive = !MyButtonIsActive;
		/// }
		///
		/// HDInterviewOptions.OnInit = function ()
		/// {
		/// 	HD$.AddCustomToolbarButton(HelloWorld,
		/// 		"http://servername/images/btn.png", "http://servername/images/btnhot.png",
		/// 		"Status Message", "Tooltip ",
		/// 		ActiveButton,
		/// 		"http://servername/images/btnActive.png", "http://servername/images/btnActiveHot.png",
		/// 		"Active Tooltip"
		/// 	);
		/// }
		/// </code>
		/// </example>
		_rpc.AddCustomToolbarButton(imageNormal,
			imageHot, statusText, tooltip, typeof funcActive == 'function', imageActive, imageActiveHot,
			tooltipActive, buttonIndex, textOnlyCaption,
			function(buttonID) {
				_eventHandlers[buttonID] = funcClick;
				if (typeof funcActive == 'function')
					_funcActives[buttonID] = funcActive;
				if (callback)
					callback(buttonID);
			}
		);
	};

	$.GetAnswerIterator = $.AnsGetIter = function(callback) {
		/// <summary>
		/// This function returns a single number value that represents the repeat index (1-based) of the current dialog.
		/// This is different than <see cref="AnsGetRptNdx"/>, which is 0-based and returns a complete set of four indexes. 
		/// As such, <see cref="AnsGetIter"/> is most useful for non-nested repeats where there is only one index that matters.
		/// </summary>
		/// <remarks>
		/// If you use this function on a nested, repeated dialog, it only returns the repeat index at the deepest level. For example, if you have one repeated dialog within another repeated dialog, the value it returns would be 1 whether you're on the first repetition of the first repeated dialog, or you're on the first repetition of the second nested dialog. In other words, <see cref="AnsGetIter"/> always returns the last non-negative number you would get from <see cref="AnsGetRptNdx"/>, plus 1.
		/// </remarks>
		/// <param name="callback">[function] A function to which the 1-based repeat index of the current repeated dialog will be passed.</param>
		_rpc.GetAnswerIterator.apply(_rpc, arguments);
	};

	$.GetAnswerRepeatIndex = $.AnsGetRptNdx = function(callback) {
		/// <summary>
		/// This function returns the full set of repeat indexes for the current dialog. When using <see cref="AnsGetVal"/> and <see cref="AnsSetVal"/>
		/// with repeated dialogs, this function allows you to get or set the values of variables in the current repetition.
		/// </summary>
		/// <param name="callback">[function] A function to which the repeat index of the current dialog will be passed. 
		/// This will be a string in the form of "0:-1:-1:-1", where the index at each repeat level is separated by a colon, 
		/// and -1 is used to represent unused levels of repetition.</param>
		_rpc.GetAnswerRepeatIndex.apply(_rpc, arguments);
	};

	$.GetAnswer = $.AnsGetVal = function(vName, ndx, callback) {
		/// <summary>
		/// This function gets the answer value for a variable. It can also get a single (indexed) value from a repeated answer.
		/// </summary>
		/// <param name="vName">[string] A variable name.</param>
		/// <param name="ndx">[string] A string of numeric values delimited by colons (:), which represents the 0-based repeat index at each level, with a limit of four levels. [optional]</param>
		/// <param name="callback">[function] A function to call after the value has been returned. It should accept one argument, which is the requested value.</param>
		/// <remarks>
		/// For more information about answers and repeat indexing, see <see href="../Concepts/HotDocs_Answers.htm">HotDocs Answers</see>.
		/// </remarks>
		_rpc.GetAnswer.apply(_rpc, arguments);
	};

	$.SetAnswer = $.AnsSetVal = function(vName, value, ndx, callback) {
		/// <summary>
		/// This function sets the answer value for a variable. It can also set a single (indexed) value in a repeated answer.
		/// </summary>
		/// <param name="vName">[string] Variable name.</param>
		/// <param name="value">[object] New value to be set. Pass <b>null</b> to specify an unanswered value.</param>
		/// <param name="ndx">[string] A string of numeric values delimited by colons (:). [optional]</param>
		/// <param name="callback">[function] A function to call after the value has been set. HD$.ScreenRefresh is a good candidate, or you can create your own that will then call HD$.ScreenRefresh. [required]</param>
		/// <remarks>
		/// For more information about answers and repeat indexing, see <see href="../Concepts/HotDocs_Answers.htm">HotDocs Answers</see>.
		/// </remarks>
		_rpc.SetAnswer.apply(_rpc, arguments);
	};

	$.GetInterviewBookmark = function(callback) {
		/// <summary>
		/// This function returns a string (up to 70 characters in length) that represents the user's current location in the interview, 
		/// including the input field that contains the focus. For example, you can call this function before the user leaves the interview,
		/// in order to submit a bookmark for the user's current location to the server along with the user's answers. 
		/// Later, you could use the <see cref="GotoInterviewBookmark"/> method to return the user to the same location in the interview.
		/// </summary>
		/// <param name="callback">[function] A function to which the requested interview bookmark will be passed.</param>
		/// <remarks>
		/// Bookmarks are simple strings, defined as follows. For variables appearing outside a dialog, the bookmark is simply
		/// the variable name.  For dialogs, a bookmark looks like this:<br/>
		/// "Dialog Name|Dialog item index|Repeat Index"
		/// <p class="Note">If the current answer set changes between the time the bookmark is created and when it is used, the
		/// bookmark may no longer be valid. For example, if you create a bookmark to the 5th repetition of a repeated dialog,
		/// but later delete answers so there are only four repetitions, that bookmark will no longer be valid. Calling
		/// <see cref="GotoInterviewBookmark"/> with an invalid bookmark string will not have any effect.</p>
		/// <p class="Tip">This function was introduced in HotDocs Server 10.</p>
		/// </remarks>
		_rpc.GetInterviewBookmark.apply(_rpc, arguments);
	};

	$.GetSnapshot = function(callback) {
		/// <summary>
		/// This function returns a snapshot string you can use to return to a certain point in the interview.
		/// </summary>
		/// <param name="callback">[function] A function to which the snapshot string will be passed.</param>
		_rpc.GetSnapshot.apply(_rpc, arguments);
	};

	$.GetXMLAnswers = function(callback) {
		/// <summary>
		/// This function returns an XML string of answers from the current interview. 
		/// For example, you could use this string to overlay the current interview's answers on top of an existing answer file, 
		/// thus updating the answer file with any answers that have been entered during the interview.
		/// </summary>
		/// <param name="callback">[function] A function to which the requested string of XML answers will be passed.</param>
		_rpc.GetXMLAnswers.apply(_rpc, arguments);
	};

	$.GetAllXMLAnswers = function (callback) {
		/// <summary>
		/// This function returns an XML string of answers from the current and all previous interviews. 
		/// For example, you could use this string to overlay the current and previous interviews' answers on top of an existing answer file, 
		/// thus updating the answer file with any answers that have been entered during the interviews.
		/// </summary>
		/// <param name="callback">[function] A function to which the requested string of XML answers will be passed.</param>
		_rpc.GetAllXMLAnswers.apply(_rpc, arguments);
	};

	$.GotoInterviewBookmark = function(str, callback) {
		/// <summary>
		/// This function navigates the user to a specific dialog in the interview, and moves the focus to a designated field within that dialog. 
		/// If the bookmark is invalid, or the location is no longer part of the interview, no navigation takes place.
		/// </summary>
		/// <remarks><p class="Note">This function was introduced in HotDocs Server 10.</p></remarks>
		/// <param name="str">[string] An interview bookmark string, which was generated by calling GetInterviewBookmark.</param>
		/// <param name="callback">[function] A function to call after the interview has navigated to the specified bookmark.</param>
		_rpc.GotoInterviewBookmark.apply(_rpc, arguments);
	};

	$.InvokeButton = function(buttonId, callback) {
		/// <summary>
		/// This function invokes the 'OnClick' function for one of the toolbar or navigation bar buttons (either built-in or custom), 
		/// even if the button has been hidden. For example, if you create your own "Finish" button, you may want to hide the built-in 
		/// "Finish" button but still have your button perform the same action by invoking the hidden "Finish" button. 
		/// </summary>
		/// <param name="buttonId">[string] This is the button's unique identifier. See Remarks for list of built-in interview buttons, or supply a custom button ID as returned from the 
		/// <see cref="HD_0024.AddCustomNavButton">HD$.AddCustomNavButton</see> or <see cref="HD_0024.AddCustomToolbarButton">HD$.AddCustomToolbarButton</see> methods.
		/// </param>
		/// <param name="callback">[function]A callback function.</param>
		/// <remarks>
		/// InvokeButton cannot be called in cases where the interview is not available for user interaction, such as when
		/// the ServerResponseVisible API returns true.
		/// <para>The following built-in button IDs can be provided for the <paramref name="buttonId"/> parameter:
		/// <include file="../JavaScriptAPI/jsapi.xml" path="Help/Table/InvokeButton/ButtonIds/*"/>
		/// </para>
		/// </remarks>
		_rpc.InvokeButton.apply(_rpc, arguments);
	};

	$.ScreenRefresh = function(callback) {
		/// <summary>
		/// This function refreshes the screen to update answers in the variable fields that have changed.
		/// </summary>
		/// <param name="callback">[function]A callback function.</param>
		/// <example><p>The following JavaScript function calls <see cref="AnsSetVal"/> to set some answer values and then calls <see cref="ScreenRefresh"/> to update the variable fields:</p>
		/// <code lang="js">
		/// function SetVariables(index) {
		/// 	HD$.AnsSetVal("Employee Name",EmpName[index]);
		/// 	HD$.AnsSetVal("Employee Gender",EmpGender[index]);
		/// 	HD$.AnsSetVal("Employee Birth Date",new Date(EmpBirth[index]));
		/// 	HD$.ScreenRefresh();
		/// }</code>
		/// </example>
		_rpc.ScreenRefresh.apply(_rpc, arguments);
	};

	$.SetButtonVisibility = function(buttonId, bVisible, callback) {
		/// <summary>
		/// This function lets callers hide individual buttons on the toolbar or navigation bar. It can also be used to hide the entire toolbar or navigation bar.
		/// </summary>
		/// <param name="buttonId">[string] This is the button's unique identifier. See Remarks for list of built-in interview buttons, or supply a custom button ID as returned from the 
		/// <see cref="HD_0024.AddCustomNavButton">HD$.AddCustomNavButton</see> or <see cref="HD_0024.AddCustomToolbarButton">HD$.AddCustomToolbarButton</see> methods.
		/// </param>
		/// <param name="bVisible">[bool] Indicates whether or not the button, toolbar, or navigation bar should be made visible.</param>
		/// <param name="callback">[function]A callback function.</param>
		/// <remarks>
		/// The following built-in button IDs can be provided for the <paramref name="buttonId"/> parameter:
		/// <include file="../JavaScriptAPI/jsapi.xml" path="Help/Table/SetButtonVisibility/ButtonIds/*"/>
		/// </remarks>
		/// <example>The following example defines an <i>OnInit</i> function (which you can set as the <see cref="HDInterviewOptions.OnInit"/> member of
		/// <see cref="HDInterviewOptions"/>) where we set the initial visibility of the "next unanswered dialog" button to false (hidden): 
		/// 
		/// <code lang="js">
		/// function OnInit()
		/// {
		/// 	HD$.SetButtonVisibility("HDNextUnans", false);
		/// }
		/// </code>
		/// </example>
		_rpc.SetButtonVisibility.apply(_rpc, arguments);
	};

	$.SetToolbarButtonImage = function(buttonId, imageNormal, imageHot, imageActive, imageActiveHot, callback) {
		/// <summary>
		/// This function lets callers specify alternate images for built-in toolbar buttons. 
		/// It should be called before the toolbar is rendered for the first time, such as in your <see cref="HDInterviewOptions.OnInit">HDInterviewOptions.OnInit</see> method.
		/// </summary>
		/// <remarks><p class="Caution">Silverlight interviews support only .PNG and .JPG images. If you intend to use the Silverlight interview format, make sure to use a supported image type.</p></remarks>
		/// <param name="buttonId">[string] This is the unique identifier for one of the following built-in toolbar buttons:
		/// HDAnsSummary, HDSaveAnswers, HDOutlineToggle, HDOutlineDropDown, HDInstUpdateToggle, HDSinglePageToggle, HDResourcePaneToggle, HDDocPreview.
		/// HDIcon can be used to replace the HotDocs icon. In this case only imageNormal will be used.
		/// <p class="Note">HDDocPreview currently doesn't work in Silverlight. Use your custom image names when calling AddCustomToolbarButton instead. </p>
		/// </param>
		/// <param name="imageNormal">[string] This is the normal image for the button. <p class="Tip">To avoid ambiguity, you should use a full URL for this and all other image files passed to this function.</p></param>
		/// <param name="imageHot">[string] This is the image for the button when the mouse is hovered over it.</param>
		/// <param name="imageActive">[string] This is the image for the button when it is active.</param>
		/// <param name="imageActiveHot">[string] This is the image for the button when it is active and the mouse is hovered over it.</param>
		/// <param name="callback">[function]A callback function.</param>
		_rpc.SetToolbarButtonImage.apply(_rpc, arguments);
	};

	// ********** Private functions **********

	// Array.prototype does not have indexOf in IE8, and we don't want this file to be dependent on jQuery.
	function indexOf(array, value) {
		for (var i = 0; i < array.length; i++) { // IE8 does not support getOwnPropertyNames.
			if (array[i] === value) {
				return i;
			}
		}
		return -1;
	}

	function endsWith(str, suffix) {
		return str.indexOf(suffix, str.length - suffix.length) !== -1;
	}

	function getHeight() {
		if (typeof (window.innerWidth) == 'number') {
			return window.innerHeight;
		} else if (document.documentElement && document.documentElement.clientHeight) {
			return document.documentElement.clientHeight;
		} else if (document.body && document.body.clientHeight) {
			return document.body.clientHeight;
		}
		return 0;
	}

	function updateInterviewSize() {
		if (_container) {
			var interviewHeight = getHeight() - _container.getAttribute("offsetTop");

			if (!_iframeID) // Hack to account for IE's initial mis-rendering of JS interview.
				interviewHeight--; // we make the container 1 pixel too short, then re-size later to correct size.

			_container.style.height = interviewHeight;
			if (_iframeID) {
				document.getElementById(_iframeID).style.height = interviewHeight;
			}
		}
	}

	// Creates the actual iframe and interview.
	// This is called by HD$.CreateInterviewFrame and by the rpc-facing RecreateInterviewFrame.
	function doCreateInterviewFrame(sessionID) {
		updateInterviewSize();
		_container.style.overflow = 'hidden';
		_container.style.margin = '0px';

		var HDFunctions = {};
		for (var f in $) {
			if (typeof $[f] == "function" && f != "CreateInterviewFrame" && f.charAt(0) != "_")
				HDFunctions[f] = {};
		}

		var config = {
			remote: $.CloudServicesAddress + '/Embedded/InterviewContainer.aspx?sessionid=' + sessionID,
			container: _container,
			onReady: function() {
				_iframeID = _container.getElementsByTagName('iframe')[0].id;
				if (typeof _onFrameReady == 'function')
					_onFrameReady(_iframeID);
			},
			props: {
				style: {
					width: '100%',
					height: '100%'
				}
			}
		};

		var jsonRpcConfig = {
			remote: HDFunctions,
			local: {
				RelayEvent: {
					method: function(eventName, eventArgs, callback) {
						if (_eventHandlers[eventName]) {
							var handler = _eventHandlers[eventName];
							if (typeof handler == 'function') {
								handler(eventArgs);
							}
							else {
								for (var i in _eventHandlers[eventName]) {
									handler[i](eventArgs);
								}
							}
						}
						callback(eventArgs);
					}
				},

				// Fire-and-forget -- no callback.
				RecreateInterviewFrame: {
					method: function() {
						_container.removeChild(_container.getElementsByTagName('iframe')[0]);
						_iframeID = null;
						// Presumably everything in _eventHandlers was registered in response to the old frame's onFrameReady or OnInit event.
						// Since the new frame will also have an onFrameReady event and an OnInit, these handlers will get registered again.
						// In order to avoid duplicates, we clear out the _eventHandlers, _funcActives, and _funcEnableds tables.
						_eventHandlers = {};
						_funcActives = {};
						_funcEnableds = {};
						doCreateInterviewFrame(_sessionID);
					}
				},

				// Fire-and-forget -- no callback.
				UpdateInterviewSize: {
					method: function() {
						updateInterviewSize();
					}
				},

				FuncActive: {
					method: function(buttonID, callback) {
						callback(_funcActives[buttonID](buttonID));
					}
				},

				FuncEnabled: {
					method: function(buttonID, callback) {
						callback(_funcEnableds[buttonID](buttonID));
					}
				},

				GetInterviewOptions: {
					method: function(callback) {
						if (HDInterviewOptions.OnInit) {
							_eventHandlers.OnInit = [HDInterviewOptions.OnInit];
							HDInterviewOptions.OnInit = true; // EasyXDM won't be able to serialize the function, so temporarily replace it with a bool
							callback(HDInterviewOptions);
							HDInterviewOptions.OnInit = _eventHandlers.OnInit[0]; // Restore HDInterviewOptions to its original state
						}
						else {
							callback(HDInterviewOptions);
						}
					}
				}
			}
		}

		_rpc = new easyXDM.Rpc(config, jsonRpcConfig);

		window.onresize = updateInterviewSize;
	}
})(HD$);
