<?php
session_start();
require_once("server/constants.php");
require_once("server/https.php");
?>
<!DOCTYPE html>
<html lang="en-us" ng-app="SnapApp" ng-controller="GlobalController">
	<head>
		<title ng-bind="pageTitle">Arkansas Legal Snap</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="description" content="{{pageDescription}}">
		<link href="https://fonts.googleapis.com/css?family=Nunito:300" type="text/css">
		<link rel="stylesheet" type="text/css" href="client/styles/master-min.css?v=<?php echo STATIC_VERSION; ?>">
		<?php
			// Non-minified Angular source locally for better error messaging
			$min = (IS_LOCAL) ? "" : ".min";
		?>
		<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.2/angular<?php echo $min; ?>.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.2/angular-route<?php echo $min; ?>.js"></script>
	</head>
	<body ng-class="[focusMode, currentColor, mobileNavOpen]">

		<ng-include src="'client/app/partials/header/header.html'"></ng-include>

		<div class="main-content">
			<ng-view autoscroll="true"></ng-view>
		</div>

		<ng-include src="'client/app/partials/footer/footer.html'"></ng-include>

		<div class="banner loading-banner" ng-class="{'active': loading}">Loading...</div>
		<div class="banner local-banner" ng-class="{'active': isLocal}">Dev</div>
		<div class="banner admin-banner" ng-class="{'active': isAdmin}"><a ng-href="#/admin">Admin</a></div>

		<script src="client/js/master-min.js?v=<?php echo STATIC_VERSION; ?>"></script>
		<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
		<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-47017741-2', 'auto');
		ga('send', 'pageview');
		</script>
	</body>
</html>
