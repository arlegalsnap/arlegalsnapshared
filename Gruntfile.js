module.exports = function(grunt) {

	grunt.initConfig({

    	pkg: grunt.file.readJSON("package.json"),
    	less: {
      		development: {
        		options: {
          			paths: ["client/styles/less"],
          			compress: true
        		},
        		files: {
          			"client/styles/master-min.css": "client/styles/less/master.less"
        		}
      		}
    	},
    	uglify: {
      		my_target: {
        		files: {
          			"client/js/master-min.js": ["client/vendor/*.js", "client/app/global/modules.js", "client/app/*/*.js", "client/app/*/*/*.js", "client/app/*/*/*/*.js"]
        		}
      		}
    	},
    	watch: {
      		options: {
        		livereload: true
      		},
      		css: {
        		files: ["client/styles/less/*.less", "client/styles/less/modules/*", "client/styles/less/pages/*", "client/styles/bootstrap/less/*", "client/styles/bootstrap/less/mixins/*"],
        		tasks: ["less"]
      		},
      		scripts: {
        		files: ["client/vendor/*.js", "client/app/*/*.js", "client/app/*/*/*.js", "client/app/*/*/*/*.js"],
        		tasks: ["uglify"]
      		}
    	}
  	});

	grunt.loadNpmTasks("grunt-contrib-less");
	grunt.loadNpmTasks("grunt-contrib-watch");
	grunt.loadNpmTasks("grunt-contrib-uglify");

	grunt.registerTask("default", ["less", "uglify", "watch"]);
};
